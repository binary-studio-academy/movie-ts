export interface IResults {
  overview: string,
  poster_path: string | null,
  release_date: string,
  original_title: string,
  id: number,
  adult?: boolean,
  backdrop_path?: string,
  genre_ids?: number[],
  original_language?: string,
  popularity?: number,
  title?: string,
  video?: boolean,
  vote_average?: number,
  vote_count?: number,
}

export interface Movie {
  id: number,
  title: string,
  overview: string,
  poster: string | null,
  release: string,
}