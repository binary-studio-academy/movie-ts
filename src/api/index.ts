import { API_URL } from './../env';
import { IAPI } from './interfaces';

const API: IAPI = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get: async function (url): Promise<any> {
    const response: Response = await fetch(`${API_URL}${url}`, {
      method: 'GET',
    });

    if (response.ok) {
      return response.json();
    }

    return response;
  },
};

export default API;
