import { addSearchListener } from "./components/Search";
import { addLoadMoreListener } from './components/LoadMore/';
import { addPopularListener } from './components/Popular/';
import { addTopRatedListener } from './components/TopRated/';
import { addUpcomingListener } from './components/Upcoming/index';
import { updateCardListeners } from './components/Favorites/likes';
import { addFavoriteListener } from './components/Favorites/index';

export async function render(): Promise<void> {
	addPopularListener();
	addUpcomingListener();
	addTopRatedListener();
	addSearchListener();
	addLoadMoreListener();
	addFavoriteListener();

	const filmContainer:HTMLElement | null = document.querySelector('#film-container');
	const favoriteContainer:HTMLElement | null = document.querySelector('#favorite-movies');

	const observer:MutationObserver = new window.MutationObserver(() => {
		updateCardListeners();
	});

	if (filmContainer) {
		observer.observe(filmContainer, {
			childList: true,
		});
	}

	if (favoriteContainer) {
		observer.observe(favoriteContainer, {
			childList: true,
		});
	}
}