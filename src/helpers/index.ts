import { API_KEY } from './../env';
import API from "../api";
import { IResults, Movie } from '../interfaces';
import { SEARCH_TYPE } from './types';

export const filterMovieData = (element:IResults):Movie => ({ id: element.id, title: element.original_title, overview: element.overview, poster: element.poster_path, release: element.release_date});

export const createListener = (type:string, listener:EventListener, elementId = '', isDefalut= false):void => {
  const element:HTMLElement | null = document.querySelector(elementId);

	if (element) {
		element.addEventListener(type, listener);
		if (isDefalut) {
      element.click();
    }
	};
};

export const updateRandomMovie = (movieTitle:string, movieDescription:string):void => {
  const title:HTMLElement | null = document.querySelector('#random-movie-name');
  const description:HTMLElement | null = document.querySelector('#random-movie-description');

  if (title && description) {
    title.innerText = movieTitle;
    description.innerText = movieDescription;
  };
};

export const randomInteger = (min:number, max:number):number => {
  const random = min + Math.random() * (max + 1 - min);
  
  return Math.floor(random);
};

export const createHandleAction = (url:string, type:string, queryOptions = '', queryValue = ''):EventListener => {
  const response = API.get(`${url}?api_key=${API_KEY}&language=en-US&page=1${queryOptions}`);
		
  if (response) {
    const JSONFavouriteList: string | null = localStorage.getItem('FavoritesList');

    const favoritesList:number[] = JSONFavouriteList ? JSON.parse(JSONFavouriteList) : [];
  
    localStorage.setItem('type', type);
    localStorage.setItem('page', '1');

    if (type === SEARCH_TYPE) {
      localStorage.setItem('query', queryValue);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    response.then((json:any):void => {
      localStorage.setItem('totalPages', String(json.total_pages));

      const results:Array<IResults> = json.results;
      const filteredData:Array<Movie> = results.map(el => filterMovieData(el));

      const moviesContainer:HTMLElement | null = document.querySelector('#film-container');
      const moviesCards:Array<string> = filteredData.map(
        ({ id, poster, title, overview, release }:Movie):string => createCard(id, poster, title, overview, release, favoritesList.includes(Number(id)))
      );

      const randomNum:number = randomInteger(0, filteredData.length - 1);
      updateRandomMovie(filteredData[randomNum].title, filteredData[randomNum].overview);

      if (moviesContainer && moviesCards.length > 0) {
        moviesContainer.innerHTML = moviesCards.join('');
      } else if (moviesContainer) {
        moviesContainer.innerHTML = '<div>no movies found</div>'
      };
    });
  };

  return () => null;
};

export const getMore = (url:string, isSearch = false):void => {
  const query:string | null = isSearch ? localStorage.getItem('query') : '';
  const currentPage:string | null = localStorage.getItem('page');
  const totalPages:string | null = localStorage.getItem('totalPages');
  const JSONFavouriteList: string | null = localStorage.getItem('FavoritesList');

  const newPage = String(Number(currentPage) + 1);
  const favoritesList:number[] = JSONFavouriteList ? JSON.parse(JSONFavouriteList) : [];

  if (newPage && (Number(totalPages) > Number(currentPage))) {
    let response;

    if (isSearch) {
      response = API.get(`${url}?api_key=${API_KEY}&language=en-US&page=${newPage}&query=${query}`);
    } else {
      response = API.get(`${url}?api_key=${API_KEY}&language=en-US&page=${newPage}`);
    }
    
    if (response) {
      localStorage.setItem('page', newPage);

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      response.then((json:any):void => {
        localStorage.setItem('totalPages', String(json.total_pages));

        const results:Array<IResults> = json.results;
        const filteredData:Array<Movie> = results.map(el => filterMovieData(el));

        const moviesContainer:HTMLElement | null = document.querySelector('#film-container');
        const moviesCards:Array<string> = filteredData.map(
          ({ id, poster, title, overview, release }:Movie):string => createCard(id, poster, title, overview, release, favoritesList.includes(Number(id)))
        );

        const randomNum:number = randomInteger(0, filteredData.length - 1);
        updateRandomMovie(filteredData[randomNum].title, filteredData[randomNum].overview);

        if (moviesContainer && moviesCards.length > 0) {
          moviesContainer.innerHTML += moviesCards.join('');
        };
      });
    };
  };
};

export const createCard = (id:number, imgUrl:string | null, title:string, description:string, date:string, isFavourite = false, favouriteColumn = false):string => {
  return (`
    <article data-id="${id}" class="${favouriteColumn ? "col-12 p-2" : "col-lg-3 col-md-4 col-12 p-2"}">
      <div class="card shadow-sm">
        <img
          src=${imgUrl ? `https://image.tmdb.org/t/p/original${imgUrl}` : "https://i.ibb.co/tcXq1DM/Vintage-movie-camera-Cinema-industry-retro-concept-symbol-Vector-illustration-in-trendy-flat-style-d.jpg"}
        />
        <svg
          data-id="${id}"
          style="${isFavourite ? "fill: red;" : "fill: rgba(255, 0, 0, 0.47);"}"
          xmlns="http://www.w3.org/2000/svg"
          stroke="red"
          width="50"
          height="50"
          class="bi bi-heart-fill position-absolute p-2 like"
          viewBox="0 -2 18 22"
        >
          <path
            fill-rule="evenodd"
            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
          />
        </svg>
        <div class="card-body">
          <h4 class="title">${title}</h4>
          <p class="card-text truncate">
            ${description}
          </p>
          <div
            class="
              d-flex
              justify-content-between
              align-items-center
            "
          >
            <small class="text-muted">${date}</small>
          </div>
        </div>
      </div>
    </article>
  `);
};