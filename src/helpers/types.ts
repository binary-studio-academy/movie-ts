export const SEARCH_TYPE = 'search';
export const POPULAR_TYPE = 'popular';
export const TOP_RATED = 'top_rated';
export const UPCOMING = 'upcoming';