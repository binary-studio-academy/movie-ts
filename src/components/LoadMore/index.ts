import { SEARCH_TYPE, POPULAR_TYPE, TOP_RATED, UPCOMING } from '../../helpers/types';
import { getMore } from '../../helpers/index';

const handleClick = ():EventListener => {
  const loadMoreType:string | null = localStorage.getItem('type');

  switch (loadMoreType) {
    case SEARCH_TYPE: 
      getMore('/search/movie', true);
      break;
    case POPULAR_TYPE:
      getMore('/movie/popular');
      break;
    case TOP_RATED:
      getMore('/movie/top_rated');
      break;
    case UPCOMING:
      getMore('/movie/upcoming');
      break;
  };

  return () => null;
};

export const addLoadMoreListener = ():void => {
  const loadMoreButton:HTMLElement | null = document.querySelector('#load-more');

  if (loadMoreButton) {
    loadMoreButton.addEventListener('click', handleClick)
  };
};