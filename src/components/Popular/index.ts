import { POPULAR_TYPE } from '../../helpers/types';
import { createListener, createHandleAction } from '../../helpers/index';

const handleClick = ():EventListener => createHandleAction('/movie/popular', POPULAR_TYPE);

export const addPopularListener = ():void => createListener('click', handleClick, '#popular', true);