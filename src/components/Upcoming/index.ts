import { UPCOMING } from '../../helpers/types';
import { createListener, createHandleAction } from '../../helpers/index';

const handleClick = ():EventListener => createHandleAction('/movie/upcoming', UPCOMING);

export const addUpcomingListener = ():void => createListener('click', handleClick, '#upcoming', false);