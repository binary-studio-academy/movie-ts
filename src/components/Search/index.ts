import { SEARCH_TYPE } from '../../helpers/types';
import { createHandleAction, createListener } from '../../helpers/index';

const handleSearch = ():EventListener => {
	const searchInput:HTMLInputElement | null = document.querySelector('#search');

	if (searchInput && searchInput.value.length > 0) {
		createHandleAction('/search/movie', SEARCH_TYPE, `&query=${searchInput.value}`, searchInput.value);
	};

	return () => null;
};

export const addSearchListener = ():void => createListener('submit', handleSearch, '#searchForm', false);