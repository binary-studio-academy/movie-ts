import { TOP_RATED } from '../../helpers/types';
import { createListener, createHandleAction } from '../../helpers/index';

const handleClick = ():EventListener => createHandleAction('/movie/top_rated', TOP_RATED);

export const addTopRatedListener = ():void => createListener('click', handleClick, '#top_rated', false);
