import { API_KEY } from './../../env';
import API from '../../api';
import { createListener, createCard, filterMovieData } from '../../helpers/index';
import { Movie, IResults } from '../../interfaces';

const addFavoritesCard = async (favoritesList:number[]):Promise<void> => {
  const favoriteContainer:HTMLElement | null = document.querySelector('#favorite-movies');
  const favoritesCards:string[] = [];

  for (const id of favoritesList) {
    const response:IResults = await API.get(`/movie/${id}?api_key=${API_KEY}&language=en-US`);
    const movie:Movie = filterMovieData(response);

    if (response) {
      const card:string = createCard(id, movie.poster, movie.title, movie.overview, movie.release, true, true);
    
      favoritesCards.push(card);
    };
  };

  if (favoriteContainer && favoritesCards.length > 0) {
    favoriteContainer.innerHTML = favoritesCards.join('');
  };
};

const handleClick = ():EventListener => {
  const favoriteContainer:HTMLElement | null = document.querySelector('#favorite-movies');
  const JSONList:string | null = localStorage.getItem('FavoritesList');
  const favoritesList:number[] = JSONList ? JSON.parse(JSONList) : [];

  if (favoritesList.length > 0) {
    addFavoritesCard(favoritesList);
  } else if (favoriteContainer) {
    favoriteContainer.innerHTML = "<p>You have not added movies to favorites yet</p>";
  };

  return () => null;
};

export const addFavoriteListener = ():void => createListener('click', handleClick, '.navbar-toggler');