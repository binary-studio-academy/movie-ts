const addLike = (movieId:number, favoritesList:number[]) => {
  favoritesList.push(movieId);

  localStorage.setItem('FavoritesList', JSON.stringify(favoritesList));
}

const removeLike = (movieId:number, favoritesList:number[]) => {
  const newFavoritesList:number[] = favoritesList.filter(id => id !== movieId);
  const favoriteContainer:HTMLElement | null = document.querySelector('#favorite-movies');

  if (favoriteContainer) {
    const movieCard:HTMLElement | null = favoriteContainer.querySelector(`article[data-id="${movieId}"]`);
    const cardRegex = new RegExp(`<article data-id="${movieId}" class="col-12 p-2">\\s*([\\s\\S]*?)\\s*</article>`, 'g');

    if (movieCard && favoritesList.length > 1) {
      favoriteContainer.innerHTML = favoriteContainer.innerHTML.replace(cardRegex, '');
    } else {
      favoriteContainer.innerHTML = '<p>You have not added movies to favorites yet</p>';
    };
  };

  localStorage.setItem('FavoritesList', JSON.stringify(newFavoritesList));
}

const handleClick = (ev:Event):EventListener => {
  const likeButton = ev.currentTarget as HTMLElement;

  if (likeButton) {
    const movieId:number | undefined = Number(likeButton.dataset['id']);
    const JSONList:string | null = localStorage.getItem('FavoritesList');
    const favoritesList:number[] = JSONList ? JSON.parse(JSONList) : [];

    if (movieId && favoritesList.length > 0) {
      const isExist:boolean = favoritesList.some(id => id === movieId);

      if (isExist) {
        const allLikeButtons:NodeList | null = document.querySelectorAll(`svg[data-id="${movieId}"]`);

        if (allLikeButtons && allLikeButtons.length > 0) {
          allLikeButtons.forEach(item => {
            const element = item as HTMLElement;

            element.style.fill = '#ff000078';
          });

          removeLike(movieId, favoritesList);
        } else {
          likeButton.style.fill = '#ff000078';

          removeLike(movieId, favoritesList);
        };
      } else {
        likeButton.style.fill = 'red';

        addLike(movieId, favoritesList);
      };
    } else if (movieId) {
      likeButton.style.fill = 'red';

      addLike(movieId, favoritesList);
    };
  };

  return () => null;
};

export const updateCardListeners = ():void => {
  const likeButtons:NodeList | null = document.querySelectorAll(".like");
    
  if (likeButtons) {
    likeButtons.forEach(el => el.addEventListener('click', handleClick));
  };
};